<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Http;
use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Http\Request;

class CardController extends Controller
{

    public function getCards() {
        $response = Http::get('https://api.hearthstonejson.com/v1/latest/enUS/cards.json');
        return $response->json();
    }

    public function index(Request $request) {
        $cards = $this->getCards();
        $dropdowns = $this->prepareDropdownValues($cards);
        
        $search = $request->input('search');
        $type = $request->input('type');
        $class = $request->input('class');
        $sort = $request->input('sort');
        $order = $request->input('order');

        if ($search) {
           $cards = $this->filter($cards, $search, 'name');
        }

        if ($type) {
            $cards = $this->filter($cards, $type, 'type');
         }

         if ($class) {
            $cards = $this->filter($cards, $class, 'cardClass');
         }

         if($sort) {
            $cards = $this->sort($cards, $sort, $order);
         }
        $cards = $this->paginate($cards);
        
        return view('welcome', ['cards' => $cards, 'classNames' => $dropdowns['classNames'], 'types' => $dropdowns['types']]);
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    private function sort($items, $property, $order) {
        $collection = collect($items);
        if ($order == 'desc') {
            $sorted = $collection->sortByDesc($property);
            $sorted->all();
            return $sorted;
        } else {
            return $items;
        }
    }

    public function filter($items, $searchText, $property) {
        $filteredItem = [];
        foreach($items as $item) {
            if (isset($item[$property]) && strpos(strtolower($item[$property]), strtolower($searchText)) !== false) {
                array_push($filteredItem, $item);
            }
        }
        return $filteredItem;
    }

    private function prepareDropdownValues($cards) {
        $classNames = [];
        $types= [];
        foreach($cards as $card) {
            if (isset($card['cardClass']) && !in_array($card['cardClass'], $classNames)) {
                array_push($classNames, $card['cardClass']);
            }

            if (isset($card['type']) && !in_array($card['type'], $types)) {
                array_push($types, $card['type']);
            }
        }
        return ['classNames' => $classNames, 'types' => $types];
    }
}
