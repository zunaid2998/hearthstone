<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <!-- Styles -->

        <style>
            body {
                font-family: 'Nunito';
            }
            .dropdown button {
                width: 100%;
            }
            nav .text-sm {
                margin-top: 20px;
            }
            nav .flex {
                display: none;
            }
            svg {
                max-width: 20px;
            }
            .row.navigation {
                padding-bottom: 50px;
            }
            .table-header {
                display: flex;
                justify-content: space-between;
            }
            .table-header button {
                border-radius: 0;
                background-color: white;
                border: 1px solid lightgrey;
                color: black;
            }
            .card {
                margin: 5px;
                background: aliceblue;
            }
        </style>
    </head>
    @php
        $param_class = request()->get('class') ?? '';
        $param_type = request()->get('type') ?? '';
        $param_search = request()->get('search') ?? '';
        $param_sort = request()->get('sort') ?? '';
        $param_order = request()->get('order') ?? '';
        $param_view = request()->get('view') ?? '';
    @endphp
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 mt-5">
                    <h2>Filters</h2>
                    <div class="row">
                        <div class="col-sm">
                            <div class="dropdown full-width">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ strlen($param_class) > 0 ? $param_class : 'Search By Class' }} 
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($classNames as $className)
                                        <a class="dropdown-item" href="/?class={{$className}}&&type={{ $param_type }}">{{$className}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="dropdown full-width">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ strlen($param_type) > 0 ? $param_type : 'Search By Types' }} 
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($types as $type)
                                        <a class="dropdown-item" href="/?type={{$type}}&&class={{ $param_class}}">{{$type}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="col-sm">
                            <form action="" name="form">
                                <input type="text" name="search" class="form-control" id="search" placeholder="Search by name" value="{{$param_search ?? ''}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @if(strlen($param_class) > 0 || strlen($param_type) > 0)
            <div class="row mt-5">
                <div class="col-sm-12">
                    @if(strlen($param_class) > 0)
                    <button type="button" class="btn btn-info">
                        {{$param_class}} 
                        <a href="/?type={{$param_type}}"><span class="badge badge-light">X</span></a>
                    </button>
                    @endif
                    @if(strlen($param_type) > 0)
                    <button type="button" class="btn btn-info">
                        {{$param_type}} 
                        <a href="/?class={{$param_class}}"><span class="badge badge-light">X</span></a>
                    </button>
                    @endif
                </div>
            </div>
            @endif
            <div class="section mt-5">
                <div class="table-header">
                    <h2>List of Cards</h2>
                    <div class="btn-group ml-3 mb-3" role="group" aria-label="Basic example">
                        <a href="/?type={{ $param_type }}&&class={{ $param_class }}&&view=list">
                            <button type="button" class="btn btn-secondary">List</button>
                        </a>
                        <a href="/?type={{ $param_type }}&&class={{ $param_class }}&&view=grid">
                            <button type="button" class="btn btn-secondary">Grid</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                @if ($param_view == 'list' or $param_view == '')
                <div class="col-sm-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">
                                    Card Name
                                    <a href="/?type={{ $param_type }}&&class={{ $param_class }}&&sort=name&&order=desc">
                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                    </a>
                                </th>
                                <th scope="col">Class</th>
                                <th scope="col">
                                    Attack
                                    <a href="/?type={{ $param_type }}&&class={{ $param_class }}&&sort=attack&&order=desc">
                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                    </a>
                                </th>
                                <th scope="col">
                                    Health
                                    <a href="/?type={{ $param_type }}&&class={{ $param_class }}&&sort=health&&order=desc">
                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                    </a>
                                </th>
                                <th scope="col">Card Type</th>
                                <th scope="col">Rarity</th>
                                <th scope="col">Artist</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($cards as $card)
                        <tr>
                            <td>{{ $card['name'] ?? '-' }}</td>
                            <td>{{ $card['cardClass'] ?? '-' }}</td>
                            <td>{{ $card['attack'] ?? '-' }}</td>
                            <td>{{ $card['health'] ?? '-' }}</td>
                            <td>{{ $card['type'] ?? '-' }}</td>
                            <td>{{ $card['rarity'] ?? '-' }}</td>
                            <td>{{ $card['artist'] ?? '-' }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>
                @endif
                @if ($param_view == 'grid')
                    @foreach ($cards as $card)
                    <div class="card col-sm-12 col-md-3">
                        <div class="card-body">
                            <h5 class="card-title">{{ $card['name'] ?? ''}}</h5>
                            <p class="card-text">
                                <b>Class:</b> {{ $card['cardClass'] ?? '' }} <br>
                                <b>Attack:</b> {{ $card['attack'] ?? '' }} <br>
                                <b>Health:</b> {{ $card['health'] ?? '' }} <br>
                                <b>Type:</b> {{ $card['type'] ?? '' }} <br>
                                <b>Rarity:</b> {{ $card['rarity'] ?? '' }} <br>
                                <b>Artist:</b> {{ $card['artist'] ?? '' }}
                            </p>
                        </div> 
                    </div>
                    @endforeach
                @endif
            </div>
            <div class="row navigation">
                <div class="col-sm-12">
                    {{ $cards->appends($_GET)->links()}}
                </div>
            </div>
        </div>
    </body>
</html>
